Datos Universidad:
* Nombre: Benem�rita Universidad Aut�noma de Puebla
* Direcci�n: Avenida San Claudio esquina con Avenida San Manuel S/N, C.P. 72570.
Actualmente estudio en esta universidad. Estoy en la Licenciatura en Ingenier�a de Tecnolog�as de la Informaci�n 
que se encuentra dentro de la Facultad de Ciencias de la Computaci�n. 
Me senti muy emocionada cuando pase el examen de admisi�n. 
Escog� esta carrera porque me llam� mucho la atenci�n su plan de estudios y porque es un carrera que ayudar� mucho al futuro. 
Actualmente curso el tercer semestre, y mi historia academica no ha sido tan buena ya que si he tenido varias fallas, pero me comprometi a 
mejorar ese asunto, he conocido buenos profesores y me agrada mucho el ambiente de la facultad. Me llama la atenci�n todo lo que organizan 
(talleres, eventos, conferencias) y la carrera para mi es bastante interesante. Aunque es un poco complicada para mi, sigo en pie de querer estudiar 
esta carrera. En unos a�os espero graduarme con un buen promedio y tener la oportunidad de viajar y trabajar en una buena empresa o hasta poner 
mi propio negocio.